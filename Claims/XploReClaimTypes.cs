﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

namespace XploRe.Security.Claims
{

    /// <summary>
    ///     Claim types defined by xplo.re IT Services that are commonly used by applications.
    /// </summary>
    public static class XploReClaimTypes
    {

        // Highest OID sub-section that has been reserved:
        // urn:oid:1.3.6.1.4.1.34460.7.0.1.1.3


        #region User Settings

        /// <summary>
        ///     The OID of a claim that specifies the configured user UI culture for the display language to use.
        /// </summary>
        /// <remarks>Claim can appear only once.</remarks>
        public const string Language = "urn:oid:1.3.6.1.4.1.34460.7.0.1.1.1.1";

        /// <summary>
        ///     The OID of a claim that specifies the configured user culture for number conversion, date formatting and
        ///     related.
        /// </summary>
        /// <remarks>
        ///     Claim can appear only once.
        ///     <para>
        ///     May not be supported by some applications, which should then use <see cref="Language" /> for both the
        ///     culture and UI culture instead.
        ///     </para>
        /// </remarks>
        public const string Culture = "urn:oid:1.3.6.1.4.1.34460.7.0.1.1.2.1";

        /// <summary>
        ///     The OID of a claim that specifies the configured user time zone.
        /// </summary>
        /// <remarks>Claim can appear only once.</remarks>
        public const string TimeZone = "urn:oid:1.3.6.1.4.1.34460.7.0.1.1.3.1";

        #endregion

    }

}
