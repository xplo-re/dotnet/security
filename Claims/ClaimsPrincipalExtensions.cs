﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Security.Claims;
using JetBrains.Annotations;
using XploRe.Runtime;


namespace XploRe.Security.Claims
{

    /// <summary>
    ///     Provides convenience lookup extensions for specific claim types.
    /// </summary>
    public static class ClaimsPrincipalExtensions
    {

        /// <summary>
        ///     Retrieves the resolved user time zone identified by the <see cref="XploReClaimTypes.TimeZone" /> claim.
        /// </summary>
        /// <param name="principal">This <see cref="ClaimsPrincipal" /> instance to retrieve the time zone from.</param>
        /// <returns>
        ///     Returns the resolved user time zone, if the corresponding <see cref="XploReClaimTypes.TimeZone" /> claim
        ///     exists and has a valid time zone name value. Otherwise the local system time zone instance retrieved
        ///     from <see cref="TimeZoneInfo.Local" /> is returned.
        /// </returns>
        [NotNull]
        public static TimeZoneInfo GetTimeZone([NotNull] this ClaimsPrincipal principal)
        {
            if (principal == null) {
                throw new ArgumentNullException(nameof(principal));
            }

            TimeZoneInfo timeZone = null;
            var timeZoneId = principal.FindFirst(XploReClaimTypes.TimeZone)?.Value;

            if (!string.IsNullOrEmpty(timeZoneId)) {
                try {
                    timeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
                }
                catch (InvalidTimeZoneException) {
                }
            }

            if (timeZone == null) {
                timeZone = TimeZoneInfo.Local;


                if (timeZone == null) {
                    throw new RuntimeInconsistencyException(
                        "{0}.{1} is null.".FormatWith(typeof(TimeZoneInfo), nameof(TimeZoneInfo.Local))
                    );
                }
            }

            return timeZone;
        }

    }

}
