﻿/*
 * xplo.re .NET
 *
 * Copyright (C) 2017, xplo.re IT Services, Michael Maier.
 * All rights reserved.
 */

using System;
using System.Security.Claims;
using JetBrains.Annotations;


namespace XploRe.Security.Claims
{

    /// <summary>
    ///     Provides extension methods to convert <see cref="DateTimeOffset" /> object time ones with repsect to a given
    ///     <see cref="ClaimsPrincipal" /> instance.
    /// </summary>
    public static class DateTimeOffsetExtensions
    {

        /// <summary>
        ///     Returns a new <see cref="DateTimeOffset" /> instance based on the current <see cref="DateTimeOffset" />
        ///     instance that represents the local time for a given <see cref="ClaimsPrincipal" />'s time zone.
        /// </summary>
        /// <param name="dateTimeOffset">The <see cref="DateTimeOffset" /> instance to convert.</param>
        /// <param name="principal">
        ///     The <see cref="ClaimsPrincipal" /> instance whose <see cref="XploReClaimTypes.TimeZone" /> claim 
        ///     determines the time zone of the result. The time zone used equals the time zone returned by
        ///     <see cref="ClaimsPrincipalExtensions.GetTimeZone" />.
        /// </param>
        /// <returns>
        ///     A new <see cref="DateTimeOffset" /> instance that represents the date and time of the current
        ///     <see cref="DateTimeOffset" /> instance converted to the time zone of the <paramref name="principal" />
        ///     with fallback to the local time of the system.
        /// </returns>
        public static DateTimeOffset ToLocalTime(
            this DateTimeOffset dateTimeOffset,
            [NotNull] ClaimsPrincipal principal)
        {
            if (principal == null) {
                throw new ArgumentNullException(nameof(principal));
            }

            return TimeZoneInfo.ConvertTime(dateTimeOffset, principal.GetTimeZone());
        }

        /// <summary>
        ///     Returns a new <see cref="DateTime" /> instance based on the current <see cref="DateTimeOffset" />
        ///     instance that represents the local time for a given <see cref="ClaimsPrincipal" />'s time zone.
        /// </summary>
        /// <param name="dateTimeOffset">The <see cref="DateTimeOffset" /> instance to convert.</param>
        /// <param name="principal">
        ///     The <see cref="ClaimsPrincipal" /> instance whose <see cref="XploReClaimTypes.TimeZone" /> claim
        ///     determines the time zone of the result. The time zone used equals the time zone returned by
        ///     <see cref="ClaimsPrincipalExtensions.GetTimeZone" />.
        /// </param>
        /// <returns>
        ///     A new <see cref="DateTime" /> instance that represents the date and time of the current
        ///     <see cref="DateTimeOffset" /> instance converted to the time zone of the <paramref name="principal" />
        ///     with fallback to the local time of the system.
        /// </returns>
        public static DateTime ToLocalDateTime(
            this DateTimeOffset dateTimeOffset,
            [NotNull] ClaimsPrincipal principal)
        {
            if (principal == null) {
                throw new ArgumentNullException(nameof(principal));
            }

            return TimeZoneInfo.ConvertTime(dateTimeOffset, principal.GetTimeZone()).DateTime;
        }

        /// <summary>
        ///     Returns a new <see cref="Nullable{DateTimeOffset}" /> instance based on the current
        ///     <see cref="Nullable{DateTimeOffset}" /> instance that represents the local time for a given
        ///     <see cref="ClaimsPrincipal" />'s time zone.
        /// </summary>
        /// <param name="dateTimeOffset">The <see cref="Nullable{DateTimeOffset}" /> instance to convert.</param>
        /// <param name="principal">
        ///     The <see cref="ClaimsPrincipal" /> instance whose <see cref="XploReClaimTypes.TimeZone" /> claim
        ///     determines the time zone of the result. The time zone used equals the time zone returned by
        ///     <see cref="ClaimsPrincipalExtensions.GetTimeZone" />.
        /// </param>
        /// <returns>
        ///     A new <see cref="Nullable{DateTimeOffset}" /> instance that represents the date and time of the current
        ///     <see cref="Nullable{DateTimeOffset}" /> instance converted to the time zone of the
        ///     <paramref name="principal" /> with fallback to the local time of the system.
        /// </returns>
        public static DateTimeOffset? ToLocalTime(
            this DateTimeOffset? dateTimeOffset,
            [NotNull] ClaimsPrincipal principal)
        {
            if (principal == null) {
                throw new ArgumentNullException(nameof(principal));
            }

            return dateTimeOffset?.ToLocalTime(principal);
        }

        /// <summary>
        ///     Returns a new <see cref="Nullable{DateTime}" /> instance based on the current
        ///     <see cref="Nullable{DateTimeOffset}" /> instance that represents the local time for a given
        ///     <see cref="ClaimsPrincipal" />'s time zone.
        /// </summary>
        /// <param name="dateTimeOffset">The <see cref="Nullable{DateTimeOffset}" /> instance to convert.</param>
        /// <param name="principal">
        ///     The <see cref="ClaimsPrincipal" /> instance whose <see cref="XploReClaimTypes.TimeZone" /> claim
        ///     determines the time zone of the result. The time zone used equals the time zone returned by
        ///     <see cref="ClaimsPrincipalExtensions.GetTimeZone" />.
        /// </param>
        /// <returns>
        ///     A new <see cref="Nullable{DateTime}" /> instance that represents the date and time of the current
        ///     <see cref="Nullable{DateTimeOffset}" /> instance converted to the time zone of the
        ///     <paramref name="principal" /> with fallback to the local time of the system.
        /// </returns>
        public static DateTime? ToLocalDateTime(
            this DateTimeOffset? dateTimeOffset,
            [NotNull] ClaimsPrincipal principal)
        {
            if (principal == null) {
                throw new ArgumentNullException(nameof(principal));
            }

            return dateTimeOffset?.ToLocalDateTime(principal);
        }

    }

}
