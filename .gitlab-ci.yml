stages:
  - build
  - pack
  - deploy

variables:
  # PROJECT CONFIGURATION
  #
  # Path to project sources, relative to the repository root. Can contain globs. Must not be empty.
  SourcePath: '.'
  # Path for build artefacts, relative to the source. Used by MSBuild implicitly.
  BaseOutputPath: 'bin'
  # Path for intermediate objects, relative to the source. Used by MSBuild implicitly.
  BaseIntermediateOutputPath: 'obj'
  # Build configuration name for debug builds. Used for test build.
  DebugBuildConfiguration: 'Debug'
  # Build configuration name for release builds. Used for test build and deployments.
  ReleaseBuildConfiguration: 'Release'

  # CI-RELATED
  #
  # Cache directory for NuGet packages, relative to project root.
  NuGetPackagesPath: '.nuget'

  # ENVIRONMENT VARIABLES
  #
  # Avoid shallow clone, as otherwise the total number of commits for versioning cannot be determined.
  GIT_DEPTH: 0
  # Path to NuGet packages cache. Used by NuGet and must be absolute.
  NUGET_PACKAGES: '$CI_PROJECT_DIR/$NuGetPackagesPath'


# Default job configuration. Caches intermediate objects such as NuGet packages per stage & branch.
default:
  image: mcr.microsoft.com/dotnet/sdk:6.0
  cache:
    # Cache per stage and branch.
    key: '$CI_JOB_STAGE-$CI_COMMIT_REF_SLUG'
    # Use latest cache if available and update cache after job is completed.
    policy: pull-push
    paths:
      # Packages dependency tree with package versions, frameworks etc., including their restore location.
      - '$SourcePath/$BaseIntermediateOutputPath/project.assets.json'
      # NuGet and MSBuild related intermediate files.
      - '$SourcePath/$BaseIntermediateOutputPath/*.csproj.nuget.*'
      # NuGet cache that contains all dependencies.
      - '$NuGetPackagesPath'


# Build project for both debug and release configurations. Ensures that the project can be built before other jobs are
# started. Build artefacts are transferred to dependent jobs.
dotnet-build:
  stage: build
  before_script:
    - dotnet restore
  script:
    - dotnet build --configuration "$DebugBuildConfiguration" --no-restore
    - dotnet build --configuration "$ReleaseBuildConfiguration" --no-restore
  artifacts:
    paths:
      - '$SourcePath/$BaseOutputPath'
      - '$SourcePath/$BaseIntermediateOutputPath/$DebugBuildConfiguration'
      - '$SourcePath/$BaseIntermediateOutputPath/$ReleaseBuildConfiguration'
    expire_in: 1 month


# Builds packages for deployment.
dotnet-pack:
  stage: pack
  needs:
    - job: dotnet-build
      artifacts: true
  before_script:
    - dotnet restore
  script:
    - dotnet pack --configuration "$ReleaseBuildConfiguration" --no-restore --no-build --include-symbols
  artifacts:
    paths:
      - '$SourcePath/$BaseOutputPath/$ReleaseBuildConfiguration/*.nupkg'
      - '$SourcePath/$BaseOutputPath/$ReleaseBuildConfiguration/*.snupkg'
    expire_in: 1 week


# NuGet base for package deployment.
.nuget-push:
  stage: deploy
  needs:
    - job: dotnet-pack
      artifacts: true
  # Do not use cache.
  inherit:
    default: false
  # Skip Git fetch.
  variables:
    GIT_STRATEGY: none
  # Git not needed, use smaller Alpine image.
  image: mcr.microsoft.com/dotnet/sdk:6.0-alpine

# Push every package to GitLab package repository.
nuget-push:gitlab:
  extends: .nuget-push
  environment:
    name: NuGet GitLab Project
  before_script:
    - dotnet nuget add source "$CI_API_V4_URL/projects/$CI_PROJECT_ID/packages/nuget/index.json"
                              --name GitLab --username gitlab-ci-token --password "$CI_JOB_TOKEN" --store-password-in-clear-text
  script:
    - dotnet nuget push "$SourcePath/$BaseOutputPath/*/*.nupkg" --source GitLab --skip-duplicate

# Push every package to staging environment.
nuget-push:staging:
  extends: .nuget-push
  environment:
    name: NuGet Staging
  script:
    - dotnet nuget push "$SourcePath/$BaseOutputPath/*/*.nupkg"
                        --source "$NUGET_STAGING_SOURCE" --skip-duplicate --api-key "$NUGET_STAGING_API_KEY"
